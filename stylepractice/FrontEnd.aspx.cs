﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stylepractice
{
    public partial class FrontEnd : System.Web.UI.Page
    {
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            

            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");
            
            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            
            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>
            List<string> front_end_code = new List<string>(new string[]{
                "<html>",
                "~<head>",
                "~</head>",
                "~<body>",
                "~~<main>",
                "~~~<p>This is my code!</p>",
                "~~</main>",
                "~</body>",
                "</html>"
            });

            int i = 0;
            foreach(string code_line in front_end_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_FrontEnd.DataSource = ds;

            /*Some formatting in the codebehind*/
            

            Code_FrontEnd.DataBind();
            
            

        }
    }
}