﻿<%@ Page Title="Portfolio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrontEnd.aspx.cs" Inherits="stylepractice.FrontEnd" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h3>Front End</h3>
    <p>Here are the projects where I contributed to the visual layout, UI, or accessibility options.</p>

    <%
        /*
         * Your assignment 2b will be transforming this DataGrid into
         * a server control called "codebox"
         */ 
        %>
    <asp:DataGrid ID="Code_FrontEnd" runat="server" CssClass="code"
        GridLines="None" Width="500px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
    </asp:DataGrid>

    <%
        /*When you wrap the DataGrid into a user control and call it on a
         * webpage it should look something like this
        
    <asp:CodeBox ID="My_FrontEnd_Code" runat="server"
        SkinId="CodeBox" code="Front_End" owner="Me">
     </asp:CodeBox>

    <asp:CodeBox ID="Teacher_FrontEnd_code" runat="server"
        SkinId="CodeBox" code="Front_End" owner="Teacher">
    </asp:CodeBox>

   */
        %>



</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
    <%
    /* This user control is defined in usercontrols/contact_sidebar.ascx
     * It is a combination of multiple server controls
     * 
     * The user control itself has a codebehind, 
     * in usercontrols/contact_sidebar.ascx.cs
     * The codebehind for the server control has access to the property "Interest"
    */
           %>
    <uctrl:contact runat="server" Interest="Front End Design" id="contact_sidebar"  />
</asp:Content>
